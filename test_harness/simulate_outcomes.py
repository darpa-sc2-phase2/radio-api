# Mandated Outcomes Test Harness (MOTH)
# This test harness simulates the delivery of mandated outcomes to the container
#
# To test mandated outcomes in interactive mode, place this file and the source file
#   in a container at /root/radio_api/test_harness/
#
# Usage: python3 simulate_outcomes.py
# 
# Requires the following files to be present in the container:
#  - /root/radio_api/test_harness/mandated_outcomes_source.json
#  - /root/radio_api/update_outcomes.sh
#
# Outputs mandated outcomes file to /root/radio_api/mandated_outcomes.json and calls
#   the Radio API script /root/radio_api/update_outcomes.sh

import json
import datetime
import time
import logging
import sys
import subprocess

def main():

    logging_filename = '/logs/moth.log'
    source_file = '/root/radio_api/test_harness/mandated_outcomes_source.json'
    destination_file = '/root/radio_api/mandated_outcomes.json'
    mo_radioapi_script = '/root/radio_api/update_outcomes.sh'

    logging.basicConfig(filename=logging_filename, level=logging.INFO)
    logger = logging.getLogger('moth')

    logger.info('Reading source file: ' + source_file)
    try:
        with open(source_file) as json_data:
            mo_events = json.load(json_data)
    except IOError as e:
        logger.error('Error reading ' + source_file)
        logger.error(e)
        return
    except ValueError as e:
        logger.error('Error parsing ' + source_file)
        logger.error(e)
        return
    except Exception as e:
        logger.error('Unexpected error parsing ' + source_file + ':' + str(sys.exc_info()[0]))
        logger.error(e)
        return
    logger.info('Parsed inputs')

    start_time = datetime.datetime.today()
    logger.debug(start_time)

    logger.info('Starting mandated outcome playback...')
    for event in mo_events:

        #calculate the time between events
        t_now = datetime.datetime.today()
        t_future = start_time + datetime.timedelta(seconds=int(event['timestamp']))
        sleep_time = (t_future - t_now).total_seconds()

        if sleep_time > 0:
            logger.info('Sleeping for ' + str(sleep_time) + ' seconds')
            time.sleep(sleep_time)
        
        try:
            logger.info('Writing mandated outcome to: ' + destination_file)
            f = open(destination_file,'w')
            f.write(json.dumps(event['scenario_goals']))
            f.close()
        except IOError as e:
            logger.error('Error writing to: ' + destination_file)
            logger.error(e)
            return
        except Exception as e:
            logger.error('Unexpected error writing to ' + destination_file + ':' + str(sys.exc_info()[0]))
            logger.error(e)
            return

        logger.info('Calling update_outcomes.sh')
        ret = subprocess.call([mo_radioapi_script], timeout=5)
        if ret != 0:
            logger.warn("Failed to call update_outcomes.sh: RET: " + str(ret))
        else:
            logger.info("Called update_outcomes.sh: RET: " + str(ret))


if __name__ == "__main__":
    main()
